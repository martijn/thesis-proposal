%   - embedded (deep embedding)
%   - one sentence: contrast with shallow
%   - collection oriented language (arrays/lists/etc)
%   - "maps/folds are clear in what they do where as a loop needs to be
%      analyssed arbitrarily complicated to find out what they do(?)"
%   - structure -> optimisations (for example)

\subsection{Accelerate}\label{subsec:accelerate}
% - Why was it made (short)
To understand more about Accelerate, we first need to address the problem in its
host language: Haskell. Haskell is a functional, higher order programming
language. These languages have a mathematical structure rather than an
instruction oriented structure in languages such as C and Rust. This makes it
possible to more easily reason about programs.

One of Haskell's main features in favour of reasoning is that it is \emph{pure}
by default. This means that its functions can not have \emph{side effects} such
as mutability, input or output -- so no missiles are accidentally fired. A major
drawback of purity on the performance is that variables can only be bound once.
For example: Suppose we want to increment the first element element in a vector
\inlhs{xs = [1,2,3]} by one. We can not do this in-place. The only way to do so,
is to map \inlhs{(+1)} onto the first element in \inlhs{xs}, copy the rest of
\inlhs{xs} and then bind it to another name: \inlhs{ys = head xs + 1 : tail xs}.
What happens in memory is that a new block of memory is allocated to house
\inlhs{ys}, the desired, updated value is inserted in the head and the tail is
copied over from \inlhs{xs}. This is in the spirit of higher order (functional)
languages: We prefer predictability, abstraction and reasoning above all. But
programs need to be able to perform a task to be useful, and in production we
want the task to be performed quickly.

Specifically for Haskell, programmers have no direct control over how a result
is executed, and thus might find it difficult to make programs more performant.
Accelerate seeks to alleviate this problem. It is an embedded \gls{dsl} for
\emph{describing} parallel computations and is collection oriented, primarily
targeting arrays. I will briefly describe what an embedded language is and how
they are implemented, and then explain how a program description in Accelerate
is executed.

% - How is it implemented (extensive with examples)
Let us consider embedding a language in Haskell that generates Javascript. In a
\emph{deep embedding} we would have a datatype such as \inlhs{data JSExpr = ..|
JSAdd JSValue JSValue |..}. Suppose that we have sub-tree with a value of
\inlhs{JSAdd (JSInt 1) (JSInt 2)}. On its own it does not compute anything, it
only describes the structure of the embedded program. To compute something we
would have to write a \emph{semantics} that evaluates this expression. This can
be an evaluator, or a function that converts it to pretty-printed Javascript. In
contrast, we can have a \emph{shallow embedding} where we do not have an
intermediate representation but work with the semantics directly. Continuing
from the example in the previous paragraph -- If we want to emit Javascript
code, then the only thing we can write is \inlhs{add x y = var x ++ " + " ++ var
y}. For an evaluator we could write \inlhs{add x y = var x + var y}, for some
implementation of \inlhs{var}. In short, the benefit from a deep embedding is
that we can have multiple semantics and that we can do program transformations.
With a shallow embedding there is only one semantics that we are working with
directly. Program transformations in a shallow embedding are very unpractical,
but not impossible.

% HOAS
Accelerate embeds functions using \gls{hoas} which makes it possible to
piggy-back on \gls{ghc} for type checking. Functions are lambda ($\lambda$)
expressions and have at most one argument each i.e. they are \emph{curried}. The
front end converts these to a nameless representation using De-Bruijn
indices. These are integers, starting from $0$, specifying how many nested
levels of \lexpr s ago the variable has been bound.  As an example let us
consider the function \inlhs{add x y = x + y}. After one level of currying this
becomes: \inlhs{add x = \y -> x + y}, and after two \inlhs{add = \x -> \y -> x +
y}.  \note{We use $\$$ to denote a reference and have gone from Haskell-syntax
to mathematical syntax.} Now we remove the names and replace them by how many
\lexpr s we have to go up to find where it was bound $(+) = \lambda. \left(
\lambda. \$1 + \$0 \right)$.

% - How is it run (short, with examples)
As stated before, a program representation in Accelerate does not compute
anything on its own. We have to write an evaluator, or \emph{structural
operational semantics}, that describes how each section in the program should
advance to the next. For Accelerate we have one such semantics: A Haskell based
interpreter. It runs an Accelerate program as-is, single threaded and with the
overhead of \gls{ghc}. We also have other semantics that generate code: One
converts an Accelerate program to an \gls{llvm} program for running on a
multi-core \gls{cpu}, the other does the same but runs on Nvidia \gls{gpu}s.

% - GPU/cpu computation model (not very important, just 1 paragraph)
\subsection{The GPU/CPU computation model}\label{subsec:gpu-cpu-computations}
General purpose computing on a \gls{gpu} differs from computing on a \gls{cpu}.
A \gls{gpu} is a \gls{simd} device which is specialised for single computations
on multiple pieces of data -- such as vectors -- simultaneously. A \gls{cpu} on
the other hand, is general hardware for doing different computations on single
pieces of data -- such as numbers. With the demand for an ever increasing amount
of computing power and the end of Moore's law in sight, many have sought to do
massively parallel general purpose computation on a \gls{gpu}.

Because there are multiple computing devices and memories are available, these
devices are usually set up in a master-slave topology. A \gls{gpu} typically
has its own memories and in typically supports \gls{dma} to access the main
system memory. The standard work flow is that the master -- the \gls{cpu} --
delegates work to the slave -- in our case this is the \gls{gpu}. The slaves
memories are mapped into the main memory so that the system can exchange data
with the \gls{gpu}. This memory is still synchronised to the \gls{gpu} and it is
this synchronisation that causes many performance issues due to the Von-Neumann
bottleneck.

The Von-Neumann bottleneck is a consequence of the same-named architecture that
almost all computers use today: Data is stored separately from where the
processing takes place, therefore it needs to be transferred.  Transferring
speeds depend on the width of the bus, on the clock frequency of the
interconnect and also on the speed of the memory itself. Because buses take up a
lot of real estate on a \gls{pcb} and are restricted in their length by timing
constraints and in their shape by the wave-like nature of signals*\note{For
example, an interconnect with a right angle in it will reflect high speed
signals}, heat production is proportional to clock frequency, and memory speed
has not followed Moore's law, this is the place where performance is hindered
the most and so significant performance can often be gained by minimising the
amount of memory transfers.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Terminology}
In this section some of the terminology regarding computing on a \gls{gpu} will
be explained. Wherever an example is provided we will use the vector $\textbf{x}
= [1, 2, 3]$.

\begin{itemize}
  \item \textbf{Gather} (or \textbf{backpermute}) is an indirect vector read
  operation where the elements of a sparsely populated vector are stored in
  memory in an irregular pattern.  To represent a sparsely populated vector
  $\textbf{u}$ that holds $k$ non-empty elements we need two vectors
  $\textbf{i}$ and $\textbf{v}$ of length $k$ such that $\textbf{v}_i =
  \textbf{u}_{\textbf{i}_i}$. Vector oriented processors often support hardware
  gathers. Gather is denoted as $v \leftarrow u|_v$

  \item \textbf{Map} applies a function (called the \emph{shader}) to each
  element in a vector resulting in a new vector e.g. with $f(x) = x + 1$ we get
  $\textbf{x}' = [2, 3, 4]$.

  \item \textbf{Reduce} (or \textbf{fold}) combines the elements of a vector
  into a single element, optionally with a provided identity element e.g.  with
  $f(x, y) = x + y$ and the identity element $e = 0$, we get $1 + 2 + 3 + 0 =
  6$. In this example we used a binary function but this need not be neither is
  the identity element required.

  \item \textbf{Scan} Iteratively apply a function on the pre- or post-fixes of a
  vector resulting in a new vector e.g. with $f(x, y) = x + y$ we get
  $\textbf{x}' = [1, 3, 6]$

  \item \textbf{Scatter} (or \textbf{permute}) is the opposite operation of a
  gather, also often supported by hardware. To write a sparsely populated vector
  to memory we again require two vectors $\textbf{i}$ and $\textbf{v}$ of length
  $k$ such that $\textbf{v}_{\textbf{i}_i} = \textbf{u}_i$. Scatter is denoted
  as $u|_v \leftarrow v$.

  \item A \textbf{Kernel} (or \textbf{convolution matrix}) is a matrix that is
  convoluted with the source array (usually an image) to perform specific tasks.
  Traditional examples are blurring and edge detection.
\end{itemize}

\note{\inlhs{foldl} is a reduction in Haskell and \inlhs{zipWith} is a
map-like function that works on two vectors instead of just one.}

A notable combination of the above is the \emph{map-reduce} which applies a
function and then reduces the result with another function e.g. the dot product
between two vectors is a map-reduction*: \inlhs{foldl (+) 0 (zipWith
(*) xs ys)}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Convolution}\label{subsec:convolution}
Convolution is a higher order mathematical function that is typically denoted
with an asterisk ($*$). Its type, $(*) : (\alpha \rightarrow \beta) \rightarrow
(\alpha \rightarrow \beta) \rightarrow \alpha \rightarrow \beta$, makes clear
that it has two functions as arguments and has a function as a result.  The
resulting function shows how the shape of a function is altered by another.

\begin{equation}\label{eq:convolution-definition}
(f * g)(t) = \int_{-\infty}^\infty f(\tau)g(t - \tau)\text{ d}\tau
\end{equation}

\note{I will use image, matrix and two-dimensional array interchangeably.}
Note that $g$ is \emph{reversed} around $t$. We will use a discrete form that is
related to the continuous form in \equref{convolution-definition} called
\emph{matrix convolution}. Convolution of a kernel over an image is defined to be

\begin{equation}
\label{eq:matrix-convolution-definition}
\begin{bmatrix}
  x_{11}       & x_{12} & \hdots  & x_{1n} \\
  x_{21}       & x_{22} & \hdots  & x_{2n} \\
  \vdots       & \vdots & \ddots  & \vdots \\
  x_{m1}       & x_{m2} & \hdots  & x_{mn} \\
\end{bmatrix}
*
\begin{bmatrix}
  y_{11}       & y_{12} & \hdots  & y_{1n} \\
  y_{21}       & y_{22} & \hdots  & y_{2n} \\
  \vdots       & \vdots & \ddots  & \vdots \\
  y_{m1}       & y_{m2} & \hdots  & y_{mn} \\
\end{bmatrix}
= \sum^{m-1}_{i=0} \sum^{n-1}_{j=0}
  x_{(m-i)(n-j)} y_{(1+i)(1+j)}.
\end{equation}
\note{A demonstrative proof that this general form is correct can be found in
Appendix~\ref{app:appendix-matrix-convolution-example}}

Note that we use two sums because we are operating in two dimensions and that
$y$ is reversed like $g$ in the continuous case. If we assume that the kernel
and the \gls{roi} of the image are represented by one-dimensional, row strided
vectors we can compute the convolution in Haskell with \inlhs{convolve xs ys =
sum (zipWith (*) (reverse xs) ys)}. In practice we will have to iterate over a
vector using its indices. GPUs are often optimised for convolutions so
expressing a computation as a convolution is beneficial to the performance.

The anchor of a convolution matrix is its center. Running a convolution with a
kernel of size $p \times q$ on an image with dimensions $n \times m$ requires
reading from cells with horizontal coordinates from the interval
$\lfloor-\frac{p}{2}\rfloor \leq x \leq \lfloor n + \frac{p}{2}\rfloor$ and
vertical coordinates from the interval $\lfloor-\frac{q}{2}\rfloor \leq y \leq
\lfloor m + \frac{q}{2}\rfloor$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Boundary conditions}\label{sec:boundary-conditions}
Boundary conditions are necessary for when part of a stencil falls outside of a
matrix. This happens -- continuing from the previous paragraph -- for cases
other than $p = 1$ and $q = 1$ i.e. we require a special boundary condition
because values are only defined iff their coordinate $c \in C$ where $C =
\left\{(x, y) | x \in \{0..n-1\}, y \in \{0..m-1\}\right\}$. \note{This indexing
is for actual arrays, matrices are offset by 1.} Another solution that does not
require boundary conditions but that will shrink the result is to offset each
coordinate in $C$ with $\frac{p}{2}$ for $x$ and $\frac{q}{2}$ for $y$.

The conventional ways to handle these are shown in
\figref{boundary-handling-examples-current}. We will briefly describe each
condition.

\begin{enumerate}
  \item \textit{Clamp} or \textit{extend}, takes the value of a cell at the
  boundary and extends it indefinitely. An example taken from the leftmost
  figure in \figref{boundary-handling-examples-current} is the top-left cell. It
  is extended in the light-green block in the top-left, as well as the row
  directly below it and the column directly beside it.

  \item \textit{Mirror} is intuitive where a virtual mirror is placed at each
  boundary. The values are those that would be perceived by someone looking in
  that mirror. This is equivalent to \textit{Clamp} if the stencil does not
  exceed the matrix for more than one cell.

  \item \textit{Tile} or \textit{wrap} is most intuitively seen as a tiling of
  the image. Another way to view this is that when a stencil exceeds a boundary,
  it wraps around to the other side as if the image were mapped onto a spherical
  object -- hence the name.

  \item \textit{Constant} simply returns a constant when the stencil exceeds the
  boundary.

\end{enumerate}

\inclfigure{png}{boundary-handling-examples-current}{Conventional boundary
handling. The white part is the original image. The green parts are virtual.
Green and light-green have been altered for clarity.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Example: Mean blur}
As an example we will perform a \emph{mean blur} filter which replaces a cell
value with the mean of itself and its neighbouring values. The mean blur has a
single parameter: The radius $r$ which is the side length of the kernel.  The
values of the convolution matrix are all $\frac{1}{r^2}$. Another representation
that is typical -- which is the one used in this example -- is a kernel that is
filled with only ones and is scaled by $\frac{1}{r^2}$. In the following example
$\textbf{I}$ represents the image and we use $r = 3$.

\begin{equation}
\label{eq:example-blur-0}
\textbf{I}'
=
\frac{1}{9}
\begin{bmatrix}
  1 & 1 & 1 \\
  1 & 1 & 1 \\
  1 & 1 & 1 \\
\end{bmatrix}
*
\textbf{I}
\end{equation}

\noindent In the following example we will blur a simple image of increasing
brightness, we will assume the \emph{extend} boundary condition which extends
boundary values outside the matrix bounds.

\begin{equation}
\label{eq:example-blur-1}
\textbf{I}
=
\begin{bmatrix}
  10 & 20 & 30 \\
  40 & 50 & 60 \\
  70 & 90 & 80 \\
\end{bmatrix}
\end{equation}

\noindent The first value we will blur is the $1$ in the top-left corner. The
numbers in boldface are extended values from the original image that actually
lie outside of the bounds. The other numbers are actual values from the
original.

\begin{equation}
\label{eq:example-blur-2}
\textbf{I}_{11}
=
\frac{1}{9}
\begin{bmatrix}
  1 & 1 & 1 \\
  1 & 1 & 1 \\
  1 & 1 & 1 \\
\end{bmatrix}
*
\begin{bmatrix}
  \textbf{10} & \textbf{10} & \textbf{20} \\
  \textbf{10} & 10          & 20          \\
  \textbf{40} & 40          & 50          \\
\end{bmatrix}
\end{equation}

\noindent The result of a single computation is shown in
\equref{example-blur-3}. Note that all this work is just for \emph{one pixel}
and \emph{one channel} and is truncated to be integer.

\begin{equation}
\label{eq:example-blur-3}
\textbf{I}_{11}
=
\frac{1}{9}\left( 1 \cdot 10
 + 1 \cdot 10
 + 1 \cdot 20
 + 1 \cdot 10
 + 1 \cdot 10
 + 1 \cdot 20
 + 1 \cdot 40
 + 1 \cdot 40
 + 1 \cdot 50
 \right) = 23
 \end{equation}

\noindent The complete result is shown in \equref{example-blur-4}. The result is
clearly filtered: The variance in the pixel values has decreased which is
exactly what a mean blur should do.

\begin{equation}
\label{eq:example-blur-4}
\textbf{I}'
=
\begin{bmatrix}
  23 & 30 & 36 \\
  43 & 50 & 56 \\
  63 & 70 & 76 \\
\end{bmatrix}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - related work (halide and l[yi]ft, repa(old) or massiv (which are also in
%   haskell))
%   - compare the example from the intro with the /same/ intro in Halide
% important: what is the aim of this language and how are they different from
% accelerate?
\subsection{Related work}\label{subsec:relatedwork}
Besides Accelerate there are other (embedded) languages that aim to simplify the
development of data parallel programs. We will discuss Repa, Массив (Massiv) and
Lift which are also embedded in Haskell, as well as look at Halide. The example
we will use throughout this section is the \emph{mean blur} as shown in
\lstref{accelerate-example}

\inclsrc{hs}{accelerate-example.hs}{accelerate-example}{Implementation of the
mean blur with $r=3$ in Accelerate that we will use to demonstrate other
languages.}

%%% STANDARD LANGUAGE COMPARISON PROCEDURE
% * What is the goal of the language
% * What are the differences between the language and accelerate
% * What are the similarities between the language and accelerate

\subsubsection{Repa}
...

\subsubsection{Массив}
Массив (Massiv) is a library for parallel array computations in Haskell that is
geared towards a higher performance. Massiv is not a language and thus has no
\gls{ast} that can be manipulated, but instead is a normal library.

Massiv has support for schedules that allow the user to specify how
a computation has to be run but this is limited to \inlhs{Seq} and
\inlhs{ParOn}. The former is sequential computation while the latter pins a
computation to a specific core. \inlhs{ParOn} also has some derivatives that are
agnostic about the specific core but simply state how many cores should be used
(\inlhs{ParN}).

Like Accelerate it has support for stencil operations in arbitrary,
hyper-rectangular arrays. Features that stand out are parallel folds and the
nice combinators for slicing arrays.  Unlike Accelerate, Massiv does not seem to
have support for running on external devices and always uses the Haskell run
time system. An implementation of the mean blur is shown in
\lstref{massiv-example}

\inclsrc{hs}{massiv-example.hs}{massiv-example}{Implementation of the
mean blur in Massiv that we will use to demonstrate other languages.}

\subsubsection{Lift}
...

\subsubsection{Halide}
Halide is a language embedded in C++ that focuses on very high performance and
platform independence. By writing computations mostly as recurrent relations,
Halide has a functional style to it, although it lacks other functional features
that we are used to such as strong types, type classes, monads, composability
and purity. There is limited support for types but this comes from the C++
compiler. The implementation of the mean blur in Halide is shown in
\lstref{halide-example}.

\inclsrc{cpp}{halide-example.cpp}{halide-example}{Implementation of a mean blur
in Halide. We provide no schedule so it is inferred for us by the compiler. The
function \inlc{blur} is automatically computed for all coordinates.}

Like Accelerate, Halide is \gls{jit} compiled and carries an LLVM based
\gls{jit} compiler. Optimisations for Halide reside mostly in the LLVM back-end
or are peephole optimisations, which are optimisations based on specific
programming patterns. One of the prominent features of Halide are `schedules'
and its ability to infer them. A schedule specifies
\emph{how} a result will be computed e.g. where to parallelise or when to re-use
a result. Halide is capable of inferring schedules by using an inference engine
that has been optimised with an evolutionary learning model. The quality of the
inferred schedule depends on the target platform.

Instead of using the schedule inferrer, developers are encouraged to write
their own schedules. These are usually one to three lines of code and can be
quickly changed if the performance is not satisfactory.

These schedules are core to the language and allow for exploring
different schedules in a short amount of time with a low amount of required
expertise.

Accelerate offers all the functional features because it is embedded in Haskell
but does not support user specified schedules, but instead relies solely on the
back ends for different executions. Halide demonstrates impressive results with
these schedules such as equal performance between two high-performance
implementations of a Laplacian filter, one implemented by an x86 expert in
months, the other by an intern in a day using Halide.

