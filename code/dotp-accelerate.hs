-- Note: We assume inclusion of the Elt typeclass for all type parameters. The
-- following example is specialised to 1 dimension for demonstration purposes.
-- Kinds have been omitted.
data Acc [..] where
  ...
  Elt     :: a -> Acc a
  ZipWith :: (a -> b -> c) -> Acc (Vector a) -> Acc (Vector b) -> Acc (Vector c)
  Fold    :: (a -> a -> a) -> a -> Acc (Vector a) -> Acc a
  ...

dotp :: Acc (Vector Int) -> Acc (Vector Int -> Acc (Vector Int)
dotp xs ys =
  let zs = ZipWith (*) xs ys
   in Fold (+) 0 zs

-- Simplified interpreter.
eval :: Acc (Vector a) -> a
eval (Elt v)           = v
eval (ZipWith f xs ys) = zipwith f (eval xs) (eval ys)
eval (Fold f e xs)     = foldl f e (eval xs)
