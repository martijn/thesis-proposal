__global__ void dotp_fused(       float   d_out
                          , const float * d_in1
                          , const float * d_in0
                          , const int     length
                          ) {
  d_out = 0;

  int
      ix   = blockDim.x * blockDim.x + threadIdx.x
    , grid = blockDim.x * gridDim.x
    ;

  for (; ix < length; ix += grid) {
    d_out += d_in1[ix] * d_in0[ix];
  }
}
