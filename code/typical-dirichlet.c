static void stencil(void * src, void * dst, int x, int y) {
  if (src[y][x] is a boundary cell) {
    // Dirichlet boundary value, can again depend on the side (left, right, top,
    // bottom and corners)
    dst[y][x] = ...;
  } else {
    dst[y][x] = ...; // normal stencil operation.
  }

  return;
}
