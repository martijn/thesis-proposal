-- Some implementation of type level naturals is assumed

data Nat a => PreStencil (size :: Nat) (expr :: * -> *) (res :: *) =
  PreStencil (Vector size (expr res))

type Stencil1D     i a = PreStencil i  Exp            a
type Stencil2D   j i a = PreStencil j (Stencil1D   i) a
type Stencil3D k j i a = PreStencil k (Stencil2D j i) a
-- and so on

