// Change in water velocity u in the X direction
du = - g*(dO_dx(h,s,b))
      - u[current]*d_dx(u)
      - v[current]*d_dy(u)
      - g/(Ct*Ct)*uabs*u[current]/h[current]
      + DifU*d2_dxy2(u);

// Change in water velocity v in the Y direction
dv = - g*dO_dy(h,s,b)
      - u[current]*d_dx(v)
      - v[current]*d_dy(v)
      - g/(Ct*Ct)*uabs*v[current]/h[current]
      + DifU*d2_dxy2(v);

// Updating water flow
u[current] = u[current] + du*dT;
v[current] = v[current] + dv*dT;
