blurStencil :: (Default a, Fractional a) => Stencil Ix2 a a
blurStencil = makeStencil (Sz (3 :. 3)) (1 :. 1)
  $ \ get ->
    (/9) . sum $ fmap (\ix -> get ix)
      [(-1 :. -1), (-1 :. 0), (-1 :. 1)
      ,( 0 :. -1), ( 0 :. 0), ( 0 :. 1)
      ,( 1 :. -1), ( 1 :. 0), ( 1 :. 1)
      ]
{-# INLINE average3x3Filter #-}

blur :: Array D Ix2 Double -> Array DW Ix2 Double
blur = mapStencil Edge average3x3Filter
