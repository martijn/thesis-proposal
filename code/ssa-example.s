main:                    ; main function label (entry point)
  push  %rbp            ; Save the stack base pointer (RBP)
  mov   %rsp, %rbp      ; Make the current stack pointer the new stack base pointer
  mov   $0, -4(%rbp)    ; move "0" to the address "RBP - 4". *
  mov   $3, -5(%rbp)    ; as above. Corresponds to "x = 3"   **
  movz   -5(%rbp), %eax ; move the 3 to register EAX, and extend with zeroes
  and   $0, %eax        ; bitwise and of EAX with 0
  cmp   $0, %eax        ; compare the result with 0
  je     .LBB0_2        ; if equal, jump to .LBB0_2

  mov   $0, -4(%rbp)    ; Then basic block
  jmp    .LBB0_3        ; unconditional jump to .LBB0_3

.LBB0_2:                ; Else basic block
  mov   $1, -4(%rbp)    ;

.LBB0_3:                ; Exit basic block (EBB)
  mov    -4(%rbp), %eax ;
  pop    %rbp           ; pop the base pointer off the stack
  ret                   ; return execution to where base the pointer was

; *  The -4 is because the return value is a 32 bit, or 4 byte, value.
;    addresses above RBP are local variables of the function
; ** The -5 is because the 0 is an uint8_t, which is a byte long, and
;    placed above the previously described 4 byte value.
