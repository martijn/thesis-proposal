simulateUV :: Stencil3x3 (Float, Float) -> Stencil3x3 DataPoint -> Exp DataPoint
simulateUV ((_     , cbtop,  _      ),
            (cbleft, cbcurr, cbright),
            (_,      cbbot,  _      ))

           ((_,       top,   _      ),
            (left,    curr, right   ),
            (_,       bot,  _       ))
  = lift (u_new, v_new, h_new, s_new, d_new)
 where
    du = -g * dO_dx
             - u curr * d_dx u
             - v curr * d_dy u
             - g / (ct * ct) * uabs * u curr / h_curr
             + difU * d2_dxy2 u

    dv = -g * dO_dy
             - u curr * d_dx v
             - v curr * d_dy v
             - g / (ct * ct) * uabs * v curr / h_curr
             + difU * (d2_dxy2 v)
