; compiled with: clang -O0 -emit-llvm -o test.ll -S test.c
define dso_local i32 @main() #0 {  ; main function entry point
  %1 = alloca i32, align 4         ; allocate space for a 32 bit integer on the
                                   ;  stack
  %2 = alloca i8, align 1          ; same, but 8 bit
  store i32 0, i32* %1, align 4    ; store 0 in the first (%1) integer
  store i8 3, i8* %2, align 1      ; store 3 in %2
  %3 = load i8, i8* %2, align 1    ; load %2
  %4 = zext i8 %3 to i32           ; extend it with zeros to be 32 bits instead
                                     ;  of 8, does not preserve sign bit
  %5 = and i32 %4, 0               ; bitwise and between %4 and 0
  %6 = icmp ne i32 %5, 0           ; test if %5 is not equal to 0
  br i1 %6, label %7, label %8     ; if %6 then goto label 7 else goto label 8

; <label>:7:
  store i32 0, i32* %1, align 4    ; Store 0 in %1
  br label %9                      ; goto label 9

; <label>:8:
  store i32 1, i32* %1, align 4    ; Store 1 in %1
  br label %9                      ; goto label 9

; <label>:9:                       ; Exit basic block (EBB)
  %10 = load i32, i32* %1, align 4 ; load %1
  ret i32 %10                      ; return %10
}
