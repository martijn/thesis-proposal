simulationStep ::  Acc (Matrix Int) -> Acc (Matrix Int)
simulationStep arr = fixBoundaries (stencil shader clamp arr)
  where
    fixBoundaries ::  Acc (Matrix Int) -> Acc (Matrix Int)
    fixBoundaries arr = generate (shape arr) dirichlet
      where
        dirichlet :: Exp Int -> Exp DIM2 -> Exp Int
        dirichlet v i = cond (isBoundary i) (lift v) (arr A.! i)
