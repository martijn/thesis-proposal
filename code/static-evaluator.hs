data Expr a where
  I   :: Int  -> Expr Int
  B   :: Bool -> Expr Bool
  Add :: Expr Int -> Expr Int -> Expr Int
  Eq  :: Expr a   -> Expr a   -> Expr Bool

eval :: Expr a -> a
eval e =
  case e of
    I v     -> v
    B v     -> v
    Add l r -> eval l + eval r
    Eq  l r -> eval l == eval r
