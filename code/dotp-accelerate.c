// ZipWith (*)
__global__ void zipWith(       float * d_out
                       , const float * d_in1
                       , const float * d_in0
                       , const int     length
                       ) {
  int
      ix   = blockDim.x * blockDim.x + threadIdx.x
    , grid = blockDim.x * gridDim.x
    ;

  for (; ix < length; ix += grid) {
    d_out[ix] = d_in1[ix] * d_in0[ix]; // (*) operator appears here
  }
}

// Fold (+) 0
__global__ void fold(       float out
                    , const float * d_in
                    , const int     length
                    ) {
  out = 0; // Default element

  int

      ix   = blockDim.x * blockDim.x + threadIdx.x
    , grid = blockDim.x * gridDim.x
    ;


  for (; ix < length; ix += grid) {
    out += d_in[ix] // (+) operator appears here
  }
}
