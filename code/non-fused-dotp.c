__global__ void dotp_accelerate( float out
                               , const float * d_in1
                               , const float * d_in0
                               , const int     length
                               ) {

  float * d_out = malloc(sizeof(float) * ...);
  zipWith(d_out, d_in0, d_in1, length);

  float out;
  fold(out, d_out, length);
}
