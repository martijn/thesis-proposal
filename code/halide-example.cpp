using namespace Halide;

int main(int argc, char **argv) {
  Buffer<uint32_t> img = load_image("some_image.png");

  Func blurx, blur;
  Var x, y;

  blurx(x, y) = img(x-1, y) + img(x, y) + img(x+1, y);
  blur(x, y)  = (blurx(x, y-1) + blurx(x, y) + blurx(x, y+1))/9;

  Buffer<uint32_t> out = blur.realize(img.width(), img.height());
}
