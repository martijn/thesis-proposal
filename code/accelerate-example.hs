import Data.Accelerate (...) -- constructors
import Data.Accelerate as A
import Prelude         as P

convolve3x3 :: Num a => [Exp a] -> Stencil3x3 a -> Exp a
convolve3x3 kernel ((a,b,c), (d,e,f), (g,h,i)) =
  P.sum . P.zipWith (*) kernel [i,h,g,f,e,d,c,b,a]

blur :: Num a => Acc (Matrix a) -> Acc (Matrix a)
blur = stencil (convolve3x3 meanBlurStencil) A.clamp
  where
    meanBlurStencil = ((1/9, 1/9, 1/9)
                      ,(1/9, 1/9, 1/9)
                      ,(1/9, 1/9, 1/9))
