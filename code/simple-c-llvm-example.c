int main(void) {   // main function entry point
  uint8_t x = 3;   // set x to 3
  if (x & 1 == 0)  // if x is even (which it is not)
    return 0;      // then make the exit code 0
  return 1;        // otherwise make it 1
}
