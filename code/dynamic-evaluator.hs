data Expr
  = I Int
  | B Bool
  | Add Expr Expr
  | Eq Expr Expr

eval :: Expr -> Maybe (Either Int Bool)
eval e =
  case e of
    I v -> Left v
    B v -> Right v
    Add l r -> do
      l' <- eval l
      r' <- eval r
      case (l', r') of
        (Left vl, Left vr) -> return (Left (vl + vr))
        \_                 -> fail "Incompatible types"
    Eq l r -> do
      l' <- eval l
      r' <- eval r
      case (l', r') of
        (Left vl, Left vr)   -> return (Right (vl == vr))
        (Right vl, Right vr) -> return (Right (vl == vr))
        \_                   -> fail "Incompatible types"
