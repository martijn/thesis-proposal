TEXDIR   = tex

default: $(TEXDIR)/*.tex
	latexmk -pdfxe -latexoption=-shell-escape tex/proposal.tex

.PHONY: clean
clean: | tex
	latexmk -c tex/proposal.tex
	rm -rf {,tex/}*.{aux,bbl,blg,log,pdf,toc,bak}
	rm -rf _minted-proposal
